# VS Code snippets

## Useful extensions, discussions and more
For more information, please see the [Wiki](../wikis/home).

## html.json

### Squiz Matrix
Useful SquizMatrix snippets for quickly creating forms in the newest UTAS page layout.
To use, type the appropriate prefix into a HTML-marked document in VS Code, and press `Tab`.  
>  `squiz:fs` [tab]


#### Squiz Questions [squiz:q]
Squiz Question tab stops are:
* Column count (for multi-column designs) [defaults to 1-1]
* Question Asset ID (applies in all necessary locations at once)
* Question Number within parent asset (applies in all necessary locations at once).
    
Note the use of `%question_label_` instead of `%question_name_`. This allows for Squiz to provide the 'required' astrisk, whilst still applying the appropriate text styling!

#### Squiz Fieldset [squiz:fs]
Squiz Fieldset tab stops are:
- Fieldset index (used for the `[labelled-by]` attribute, for accessibility)
- Section Asset ID title (for the fieldset heading)
- Cursor stops in the content space of the fieldset, ready for questions to be added.

#### Squiz Submit button [squiz:submit]
Just a shortcut for adding in the submit button, rather than trying to find the correct styling.